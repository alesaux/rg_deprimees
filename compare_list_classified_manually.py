import numpy as np
import glob
import matplotlib.image as mpimg
from matplotlib.pyplot import ion
import os
import csv
import matplotlib.pyplot as plt

#ion()

directory='/Users/students/Arthur/depressed_RG/data/'
array=[]


# Open file containing numax, delta nu (Yu 2018)
#with open('/Users/students/Arthur/depressed_RG/data/KIC_numax_80-150.txt','r') as txtfile :
with open('/Users/students/Arthur/depressed_RG/data/classification_manually_numax_80_150-2.txt','r') as txtfile :
  extra_param = csv.reader(txtfile, dialect='excel',delimiter=' ')

  next(extra_param)

  KIC_numax_Lisa = []
  star_type_Lisa = []

  for row in extra_param:
    while '' in row:
      row.remove('')
    KIC_numax_Lisa += [[i for i in row][0][13:21]]
    star_type_Lisa += [[i for i in row][1]]
    #print(KIC_numax)

KIC_numax_Lisa = [np.int(i) for i in KIC_numax_Lisa] 
star_type_Lisa = [np.int(i) for i in star_type_Lisa]


# Open file containing numax, delta nu (Yu 2018)
#with open('/Users/students/Arthur/depressed_RG/data/KIC_numax_80-150.txt','r') as txtfile :
with open(directory+'spectre_echelle_8bg_fixl0_Yu2018_interpolation_ALL/classification_manually.txt','r') as txtfile2 :
  extra_param2 = csv.reader(txtfile2, dialect='excel',delimiter=' ')

  #next(extra_param2)

  KIC_numax_me = []
  star_type_me = []

  for row in extra_param2:
    while '' in row:
      row.remove('')
    #print(row[0])
    KIC_numax_me += [[i for i in row][0][16:24]]
    star_type_me += [[i for i in row][1]]
    #print(KIC_numax)

KIC_numax_me = [np.int(i) for i in KIC_numax_me] 
star_type_me = [np.int(i) for i in star_type_me]

# Function to look for matching KIC ID between two files
match = lambda a, b:[[a.index(x),b.index(x)] if (x in b and x in a) else [None,None] for x in a]

index_common_KIC = match(KIC_numax_Lisa,KIC_numax_me)

star_typeL, star_typeA = [], []

KIC_common = [KIC_numax_Lisa[x] for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None] 
star_typeL  =  [star_type_Lisa[x] for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None] 
star_typeA  =  [star_type_me[x] for pos, x in enumerate([row[1] for row in index_common_KIC]) if x is not None]  

#print(KIC_common)

KIC_same_type, star_type = [], []

for ii in range(len(KIC_common)):
  if star_typeA[ii] == star_typeL[ii]:
    KIC_same_type.append(KIC_common[ii])
    star_type.append(star_typeL[ii])



np.savetxt(directory+'same_manual_classif_Arthur_Lisa.txt', np.c_[KIC_same_type,star_type], delimiter=' ', fmt=['%-s','%-s'], header='KIC Type',comments='')














