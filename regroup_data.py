from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import ion
import scipy.ndimage as sc
from PSD_PATH import *
import csv
from construct_bg import construct_bg
from gaussian_fit import gaussian_f, fit_gaussian
from skimage.transform import rescale, resize, downscale_local_mean
import copy
import pickle
plt.ion()

'''PATHS'''
# # Repertory with kepler data
# PATH_DATA_LISA  =   '/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/'
# PATH_DATA_ARTHUR    =   '/Volumes/Data_3To/Arthur/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/'
repertory     ='/Volumes/Data_3To/Arthur/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/'
repertory_old ='/Volumes/Data_3To/Arthur/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS_OLD/'
#repertory_l0  ='/Users/students/Arthur/depressed_RG/LISA_TEST_L1_DEPRESSED/FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/'
#repertory_l0  ='/Volumes/Data_3To/DATA/KEPLER/KEPLER_LC/RG_CATALOG/RG_DR25/TEST_GOOD_FIT_TASSOUL_SM/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2/RESULTS_FITS/'
repertory_l0  ='/Volumes/Data_3To/DATA/KEPLER/KEPLER_LC/RG_CATALOG/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/'
#repertory_l0 = '/Volumes/Data_3To/DATA/KEPLER/KEPLER_LC/RG_CATALOG/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_multi/RESULTS_FITS/'
repertory_noBG  ='/Volumes/Data_3To/DATA/KEPLER/KEPLER_LC/RG_CATALOG/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS_NO_BGcorrected/'

# Create a path for every fits file containing PSD
path_data         = PSD_PATH_IN_REPERTORY(repertory_l0)
path_datab        = BACK_PATH_IN_REPERTORY(repertory_noBG)
path_data_correct = BACK_CORRECT_PATH_IN_REPERTORY(repertory_l0)
path_data_l0      = L0_PATH_IN_REPERTORY(repertory_l0)
#path_datab2        = BACK_PATH_IN_REPERTORY(repertory)


# seuil background
bg = 8

# Path to file saving directory

#PATH_SAVE1 = '/Users/alesaux/CEA/Depressed_RG/data/echelle_diag_'+str(bg)+'bg_fixl0_Yu2018_numaxRafa_newGaussian/'
#PATH_SAVE2 = '/Users/alesaux/CEA/Depressed_RG/data/spectre_echelle_'+str(bg)+'bg_fixl0_Yu2018_numaxRafa_newGaussian/'

PATH_ARTHUR='/Users/students/Arthur/depressed_RG/'
PATH_LISA='/Users/lbugnet/WORK/ARTHUR_ML/'

PATH_SAVE1 = PATH_ARTHUR+'data/echelle_diag_'+str(bg)+'bg_fixl0_Yu2018_interpolation_numaxRafa_ALL/'
PATH_SAVE2 = PATH_ARTHUR+'data/spectre_echelle_'+str(bg)+'bg_fixl0_Yu2018_interpolation_ALL/'

#PATH_SAVE = '/Users/students/Arthur/depressed_RG/'


'''Open file containing numax, delta nu (Yu 2018)'''
#with open('/Users/alesaux/CEA/Depressed_RG/RG_Kepler_Yu2018.csv','r') as txtfile :

with open(PATH_LISA+'RG_Kepler_Yu2018.csv','r') as txtfile :
    extra_param = csv.reader(txtfile, dialect='excel',delimiter=',')

    next(extra_param)

    KIC_param, numaxYu, deltanuYu, amplYu = [], [], [], []

    for row in extra_param:
        while '' in row:
            row.remove('')
        KIC_param += [[np.float(i) for i in row][0]]
        deltanuYu += [[np.float(i) for i in row][5]]
        numaxYu += [[np.float(i) for i in row][4]]
        amplYu += [[np.float(i) for i in row][6]]

KIC_param = [np.int(i) for i in KIC_param]

KICl0, all_freq_l0 = [], []
for ii in range(len(path_data_l0)):
    freq_l0 = []
    KICl0.append(np.float(path_data_l0[ii][0:9]))
    pkb_file = repertory_l0+path_data_l0[ii]

  # Open file containing numax, delta nu (Yu 2018)
    with open(pkb_file,'r') as filename :
        data_l0 = csv.reader(filename, dialect='excel',delimiter=' ')

        for i in range(4):
            next(data_l0)

        for row in data_l0:
            while '' in row:
                row.remove('')
            if np.float(row[1]) == 0.0:
                freq_l0 += [[np.float(i) for i in row][2]]

    all_freq_l0.append(freq_l0)

print('length KICl0 ',len(KICl0))


# Function to look for matching KIC ID between two files
match = lambda a, b:[[a.index(x),b.index(x)] if (x in b and x in a) else [None,None] for x in a]

KICtab, numax, deltanu, freqtab, powertab = [], [], [], [], []
numaxtab, deltanutab = [], []

# Loop to extract data from each file
for ii in range(18697,len(path_data),1) :
# for ii in range(0,12,4) :
#for ii in range(1,3001,1000):
#for ii in range(len(path_data)):

    KIC_tmp, KIC_index, freq, power = [], [], [], []

    path_fits = repertory_l0+path_data[ii]
    #print(path_data[ii])
    #print(path_data[ii][14:23])
    #path_fits = '/Users/students/Arthur/depressed_RG/012405535/kplr012405535_611_COR_PSD_filt_inp.fits'
    hdu = fits.open(path_fits)[0]
    KICtab.append(np.int(path_data[ii][14:23]))
    KIC = np.int(path_data[ii][14:23])
    KIC_tmp.append(np.int(path_data[ii][14:23]))
    #KICtab.append(012405535.0)
    data = np.array(hdu.data)
    freq = data[:,0]*1e6                    #convert to muHz
    power = data[:,1]

    powertab.append(power)
    freqtab.append(freq)

    path_back = repertory_noBG+path_datab[ii]
    #path_back = '/Users/students/Arthur/depressed_RG/012405535/back_012405535.fits'
    hdul = fits.open(path_back)[0]
    data_back=hdul.data
    #print(hdul.header)

    path_back_correct = repertory_l0+path_data_correct[ii]
    #path_back = '/Users/students/Arthur/depressed_RG/012405535/back_012405535.fits'
    hduc = fits.open(path_back_correct)[0]
    data_back_correct = hduc.data
    #print(hdul.header)

    #print('KIC', KIC)

    if KICl0.count(KIC) == 0:
    #print('=0')
        continue

    freq_l0 = all_freq_l0[KICl0.index(KIC)]

    KIC_index = match(KIC_tmp,KIC_param)

    if KIC_index[0][1] == None:
        print('KIC not in Yu2018')
        continue
        print('ok')

    numax = numaxYu[KIC_index[0][1]]
    deltanu = deltanuYu[KIC_index[0][1]]
    #A = amplYu[KIC_index[0][1]]

    #numax = data_back[2]
    #deltanu = data_back[3]
    A = data_back[1]

    #print('A', A)
    #print(data_back[1])

    diff_numax_l0 = []

    for i in freq_l0:
        diff_numax_l0.append(np.abs(i - numax))

    index_diff = diff_numax_l0.index(np.min(diff_numax_l0))

    #print(diff_numax_l0)
    #print('index_diff',index_diff)

    l0 = freq_l0[index_diff]

    if deltanuYu[KIC_index[0][1]] == 0.0:
        print('delta nu = 0')
        print(deltanuYu[KIC_index[0][1]])
        continue


    fbin    = np.mean(np.diff(freq))      #frequencies in each bin
    #deltanu = np.float(data_back[3])
    #print('deltanu',deltanu)
    deltanu = round(deltanu/fbin)*fbin
    #print('deltanu aft',deltanu)
    #numax   = np.float(data_back[2])
    numax   = round(numax/(fbin))*(fbin)

    numaxtab.append(numax)
    deltanutab.append(deltanu)

star_params={
    #"numax" : numax,
    #"deltanu": deltanu,
    "numaxtab": numaxtab,
    "deltanutab": deltanutab,
    #"l0":l0,
    "freqtab": freqtab,
    "powertab": powertab,
    "KICtab": KICtab}


# f = open("/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/save_stars_params.pkl","wb")
# pickle.dump(star_params,f)
# f.close()

def save_obj(obj, name ):
    with open("/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/"+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

save_obj(star_params, save_stars_params)
