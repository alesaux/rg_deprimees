import numpy as np
from astropy.io import fits
from matplotlib.pyplot import ion
import matplotlib.pyplot as plt
#ion()

PATH='/Users/students/Arthur/depressed_RG/'
PATH='/Users/lbugnet/DATA/DEPRESSED/LISA_TEST_L1_DEPRESSED/'
'''----------------------------------------------------'''
''' Reconstruct background from Rafa's fits'''
'''----------------------------------------------------'''

'''----------------------------------------- HARVEY'''
def harvey(param,freq_muHz):
    #print(param)
    A = np.exp(param[0])
    G = np.exp(param[1])*0.5
    alpha = np.exp(param[2])
    fac=np.sqrt(2**(1/alpha)-1)
    den = 1./((fac*freq_muHz/G)**alpha+1.)
    res = A*den
    # print('harvay', res)
    return res

def harvey_bis(param, freq_muHz):
    sig2 = np.exp(2*param[0])
    p2tau = 2*np.pi*np.exp(param[1])
    alpha = 1+np.exp(param[2])

    pi_al=np.pi/alpha
    zetanorm=np.sin(pi_al)/pi_al

    den = 1/((p2tau*freq_muHz)**alpha+1.)

    res = zetanorm*sig2*p2tau*den
    return res

'''----------------------------------------- BACKGROUND'''

# define the function blocks
def white(param, res, freq_muHz):
    B=np.exp(param[0])
    res=res+B
    return res

def Harvey1(param, res, freq_muHz):
    par=param[1:4]
    res=res+harvey_bis(par,freq_muHz)
    return res

def Harvey2(param, res, freq_muHz):
    # print(param[4:7])
    par=param[4:7]
    res=res+harvey_bis(par,freq_muHz)
    return res

def Harvey2(param, res, freq_muHz):
    # print(param[4:7])
    par=param[8:11]
    res=res+harvey_bis(par,freq_muHz)
    return res

def fct_bg(param,freq_muHz):
    nx=len(freq_muHz)
    res=np.zeros((nx))
    options = {0 : white,
           1 : Harvey1,
           2 : Harvey2,
           3 : Harvey3}
    for ic in range(0,4):
        res = options[ic](param, res, freq_muHz)
    return res

def construct_bg(freq_muHz, fit_obj):
    npar=7
    param=np.zeros((npar))
    nx=len(freq_muHz)
    i0=0
    if fit_obj['reswhite']!=None:
        param[i0]=fit_obj['reswhite']
        #print(param)
        i0+=1
    if fit_obj['resharvey1'] !=None :
        param[i0:i0+3]=fit_obj['resharvey1']
        #print(param)
        i0+=3
    if fit_obj['resharvey2'] !=None:
        param[i0:i0+3]=fit_obj['resharvey2']
        #print(param)
        i0+=4
    if fit_obj['resharvey3'] !=None:
        param[i0:i0+3]=fit_obj['resharvey3']
        #print(param)
        i0+=5
    bg=fct_bg(param,freq_muHz)

    return bg

# '''----------------------------------------------------'''
# '''CALLING SEQUENCE'''
# '''----------------------------------------------------'''
#
# '''restore fiting'''
#
# hdul = fits.open(PATH+'FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/012405535/back_012405535.fits')
# data=hdul[0].data
#
# fit_obj={
# 		"reswhite" : np.log(data[0]), #bruit blanc haute freq_muHzuence
#         "respower" : None, #loi de puissance a basse freq_muHzuence
# 		"respmodeamp" : np.log(data[1]), #amplitude des modes
# 		"respmodefreq_muHz" : data[2],#freq_muHzuence des modes
# 		"respmodewidth" : np.log(data[3]), #largeur des modes
# 		"harveyaamp" : np.log(data[4]),
# 		"harveyafreq_muHz" : np.log(data[5]),
# 		"harveyawidth" : np.log(data[6]),
# 		"harveybamp" : np.log(data[7]),
# 		"harveybfreq_muHz" : np.log(data[8]),
# 		"harveybwidth" : np.log(data[9]),
#         "resharvey1" : [np.log(data[4]), np.log(data[5]), np.log(data[6])],
#         "resharvey2" : [np.log(data[7]), np.log(data[8]), np.log(data[9])]
# 	}
#
# #restor PSD
# hdul = fits.open(PATH+'FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/012405535/PSD_012405535.fits')
# data=hdul[0].data
# freq_muHz=np.transpose(data)[0]*1e6
# power=np.transpose(data)[1]
#
# #construct background
# aa=construct_bg(freq_muHz, fit_obj) #careful you should give the freq_muHzuency in muHz !
#
# plt.loglog(freq_muHz, power)
# plt.loglog(freq_muHz, aa)
# plt.ylabel('PSD')
# plt.xlabel('muHz')
