import scipy
import numpy as np
import astropy.timeseries
import matplotlib.pyplot as plt
plt.ion()
file='/Users/lbugnet/Downloads/41586_2012_BFnature11572_MOESM142_ESM.txt'

[time, RV]=np.transpose(np.loadtxt(file,  delimiter='\t', skiprows=2,  usecols=(0,1), max_rows=None))
from astropy.timeseries import LombScargle
frequency, power = LombScargle(time, RV).autopower()
plt.figure()
plt.loglog(frequency, power)
