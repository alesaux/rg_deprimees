# multiplot PSD
# Recupere les fichiers .json de Nearest_neigbors et plots les diagrammes correspondant

from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from PSD_PATH_echelle import *
import csv
import json
from PIL import Image
import copy as copy
import matplotlib as matplotlib
from tqdm import tqdm
matplotlib.rcParams['figure.figsize'] = 20, 15
# Repertory with K2 PSD data
#repertory='/Volumes/Data_3To/DATA/K2/'
repertoryN='/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/nearest_neighbors/'
# repertoryI='/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/images_ALLstars/'
repertoryI='/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/fig_echelle/'
repertoryI2='/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/fig_echelle_and_spectrum/'
# repertoryI='/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/image_vectors/'

# Create a path for every fits file containing PSD
path_neighbors = NEIGHBORS_PATH_IN_REPERTORY(repertoryN)
path_images = IMAGES_PATH_IN_REPERTORY(repertoryI)
path_images2 = IMAGES_PATH_IN_REPERTORY(repertoryI2)


# Path to file saving directory
PATH_SAVE = '/.'

########################

'''import Mosser and stello depressed stars'''
MandS=np.loadtxt('/Users/lbugnet/WORK/ARTHUR_ML/Depressed_Mosser_plus_stello_list_KIC.txt')
KIC_MS=[np.int(i) for i in MandS]

match = lambda a, b:[[a.index(x),b.index(x)] if (x in b and x in a) else [None,None] for x in b]


KICtab, similarity, KICimage, psd, psdtab, class_type = [], [], [], [], [], []
# plt.ion()
for ii in tqdm(range(0,len(path_neighbors))):
  # print(ii)
#for ii in range(0,1,1):
  KICtab= []
  path_file = repertoryN+path_neighbors[ii]
  #path_file = '/Users/students/Arthur/depressed_RG/echelle_diag_06975038_8bg_fixl0_Yu18_3.json'
  #print(path_file)
  with open(path_file) as json_file:
    neighbors = json.load(json_file)
  #print("KIC", neighbors[0]['filename'][13:21])
  #print("Length filename", len(neighbors[0]['filename']))
  for i in range(len(neighbors)):

    start = neighbors[i]['filename'].find("diag_")+ len("diag_")
    end = neighbors[i]['filename'].find("_8bg")
    # substring = s[start:end]

    KICtab.append(np.int(neighbors[i]['filename'][start:end]))
    # KICtab.append(np.int(neighbors[i]['filename'][12:20]))
    similarity.append(neighbors[i]['similarity'])

  # input('Press enter')

  KICtab  = [np.int(i) for i in KICtab]

  KIC_verif = match(KIC_MS,[KICtab[0]])
  #KICall   =  [KICall[x] for pos, x in enumerate([row[0] for row in KIC_index]) if x is not None]



 # print(KIC_verif[0])
  #if KIC_verif[0][1] == None:
  #  continue

  # print('KICtab', KICtab[0])

  #print('tab',KICtab)
  #print('MS',KIC_MS)

  #KIC_MS = KIC_MS[0:30]
  fig = plt.figure(1)
  fig, ax = plt.subplots(nrows=5, ncols=6, sharex=True, sharey=True)
  r,c = 0,0

  # fig2 = plt.figure(2)
  # fig2, ax2 = plt.subplots(nrows=5, ncols=6, sharex=True, sharey=True)
  # r2,c2 = 0,0
  for jj in range(len(KICtab)):
  # for jj in range(len(KIC_MS)):
    KICall = []
    for ii in range(0,len(path_images),1):
    #for ii in range(0,30,1):
      #print(len(path_images[ii]))
      start = path_images[ii].find("diag_")+ len("diag_")
      end = path_images[ii].find("_8bg")
      KICimage = np.int(path_images[ii][start:end])
      #print('KIC image', KICimage)
      KICall.append(np.int(KICimage))
      if KICtab[jj] == np.int(KICimage):
      # if KIC_MS[jj] == np.int(KICimage):
        #print('KICim',KICimage)
        #print('KIC_MS',len(KIC_MS))
        image_file = repertoryI+path_images[ii]
        #print(path_images[ii])
        img = Image.open(image_file)
        #a = fig.add_subplot(6,5,jj+1)
        #imgplot = plt.imshow(img)
        #print(r,c)
        if c == 6:
          break
        ax[r,c].imshow(img)
        # print(KIC_MS.count(np.int(KICimage)))
        if KIC_MS.count(np.int(KICimage)) == 1:
          ax[r,c].title.set_text('KIC:'+str(KICimage)+' (D)')
        else:
          ax[r,c].title.set_text('KIC:'+str(KICimage))
        r += 1
        if r == 5:
          c +=1
          r = 0
        plt.savefig('/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/closest_stars_plots/'+'KIC_'+str(KICtab[0])+'.png')

        """
        if c == 6:
          plt.show()
          fig, ax = plt.subplots(nrows=5, ncols=6, sharex=True, sharey=True)
          r,c = 0,0
          ax[r,c].title.set_text('KIC:'+str(KICtab[jj]))
          r += 1
          """
        #print(r)

  #KIC_index = match(KICall,KIC_MS)
  #KICall   =  [KICall[x] for pos, x in enumerate([row[0] for row in KIC_index]) if x is not None]
  #print(len(KICall))

  # plt.show()
  plt.tight_layout()
  plt.savefig('/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/closest_stars_plots/'+'KIC_'+str(KICtab[0])+'.png')

  # for jj in range(len(KICtab)):
  # # for jj in range(len(KIC_MS)):
  #   KICall = []
  #   for ii in range(0,len(path_images2),1):
  #   #for ii in range(0,30,1):
  #     #print(len(path_images[ii]))
  #     start = path_images[ii].find("diag_")+ len("diag_")
  #     end = path_images[ii].find("_8bg")
  #     KICimage = np.int(path_images[ii][start:end])
  #     #print('KIC image', KICimage)
  #     KICall.append(np.int(KICimage))
  #     if KICtab[jj] == np.int(KICimage):
  #     # if KIC_MS[jj] == np.int(KICimage):
  #       #print('KICim',KICimage)
  #       #print('KIC_MS',len(KIC_MS))
  #       # image_file = repertoryI+path_images[ii]
  #       image_file2 = repertoryI2+path_images2[ii]
  #       #print(path_images[ii])
  #       # img = Image.open(image_file)
  #       img2 = Image.open(image_file2)
  #       #a = fig.add_subplot(6,5,jj+1)
  #       #imgplot = plt.imshow(img)
  #       #print(r,c)
  #       #print(r)
  #
  #       if c2 == 6:
  #         break
  #       ax[r2,c2].imshow(img2)
  #       # print(KIC_MS.count(np.int(KICimage)))
  #       if KIC_MS.count(np.int(KICimage)) == 1:
  #         ax[r2,c2].title.set_text('KIC:'+str(KICimage)+' (D)')
  #       else:
  #         ax[r2,c2].title.set_text('KIC:'+str(KICimage))
  #       r2 += 1
  #       if r2 == 5:
  #         c2 +=1
  #         r2 = 0
  #
  # plt.tight_layout()
  # plt.savefig('/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/closest_stars_plots_with_spectrum/'+'KIC_'+str(KICtab[0])+'.png')
  #



  # print('Do you want to continue ?')
  # rep = input('Y / N ')
  # if rep == 'N' :
  # 	break
