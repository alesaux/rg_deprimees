'''pour avoir juste les  harvey !!!!!'''
# from construct_bg import *
'''pour avoir toutes les composante !!!!!'''
from construct_bg_with_gaussian import *
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import copy  as copy
# '''FALSE CALLING !!!!!!!! SEE constrcut_bg_with_gaussian de FIT_MODES '''
#300: 002424791
#200: 002578581
#50: 001161491
#20: 001720425
#10: 003550673
plt.ion()

'''restore PSD'''
hdul=fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/001294385/PSD_001294385.fits')
psd                 =   np.transpose(hdul[0].data)
frequency_Hz        =   copy.deepcopy(psd[0])
freq_muHz           =   frequency_Hz*1e6
power               =   copy.deepcopy(psd[1])
smoothed_power=np.array([np.mean(power[ii-20:ii+20]) for ii in range(len(power))])

hdul2=fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/002424791/PSD_002424791.fits')
psd2                 =   np.transpose(hdul2[0].data)
frequency_Hz2       =   copy.deepcopy(psd2[0])
freq_muHz2           =   frequency_Hz2*1e6
power2               =   copy.deepcopy(psd2[1])
smoothed_power2=np.array([np.mean(power2[ii-20:ii+20]) for ii in range(len(power2))])

hdul3=fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/002578581/PSD_002578581.fits')
psd3                 =   np.transpose(hdul3[0].data)
frequency_Hz3       =   copy.deepcopy(psd3[0])
freq_muHz3           =   frequency_Hz3*1e6
power3               =   copy.deepcopy(psd3[1])
smoothed_power3=np.array([np.mean(power3[ii-20:ii+20]) for ii in range(len(power3))])

hdul4=fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/001161491/PSD_001161491.fits')
psd4                 =   np.transpose(hdul4[0].data)
frequency_Hz4       =   copy.deepcopy(psd4[0])
freq_muHz4           =   frequency_Hz4*1e6
power4               =   copy.deepcopy(psd4[1])
smoothed_power4=np.array([np.mean(power4[ii-20:ii+20]) for ii in range(len(power4))])

hdul5=fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/003550673/PSD_003550673.fits')
psd5                 =   np.transpose(hdul5[0].data)
frequency_Hz5       =   copy.deepcopy(psd5[0])
freq_muHz5           =   frequency_Hz5*1e6
power5               =   copy.deepcopy(psd5[1])
smoothed_power5=np.array([np.mean(power5[ii-20:ii+20]) for ii in range(len(power5))])

hdul6=fits.open('/Users/lbugnet/DATA/SOLAR_LIKE/kplr011968749_166_COR_PSD_filt_inp.fits')
psd6                 =   np.transpose(hdul6[0].data)
frequency_Hz6       =   copy.deepcopy(psd6[0])
freq_muHz6           =   frequency_Hz6*1e6
power6               =   copy.deepcopy(psd6[1])
smoothed_power6=np.array([np.mean(power6[ii-20:ii+20]) for ii in range(len(power6))])

hdul7=fits.open('/Users/lbugnet/DATA/SOLAR_LIKE/kplr003656476_12_COR_PSD_filt_inp.fits')
psd7                 =   np.transpose(hdul7[0].data)
frequency_Hz7       =   copy.deepcopy(psd7[0])
freq_muHz7           =   frequency_Hz7*1e6
power7               =   copy.deepcopy(psd7[1])
smoothed_power7=np.array([np.mean(power7[ii-20:ii+20]) for ii in range(len(power7))])

hdul8=fits.open('/Users/lbugnet/DATA/SOLAR_LIKE/kplr005512589_31_COR_PSD_filt_inp.fits')
psd8                 =   np.transpose(hdul8[0].data)
frequency_Hz8       =   copy.deepcopy(psd8[0])
freq_muHz8           =   frequency_Hz8*1e6
power8               =   copy.deepcopy(psd8[1])
smoothed_power8=np.array([np.mean(power8[ii-20:ii+20]) for ii in range(len(power8))])

hdul9=fits.open('/Users/lbugnet/DATA/SOLAR_LIKE/kplr003427720_7_COR_PSD_filt_inp.fits')
psd9                 =   np.transpose(hdul9[0].data)
frequency_Hz9       =   copy.deepcopy(psd9[0])
freq_muHz9           =   frequency_Hz9*1e6
power9               =   copy.deepcopy(psd9[1])
smoothed_power9=np.array([np.mean(power9[ii-20:ii+20]) for ii in range(len(power9))])


hdul10=fits.open('/Users/lbugnet/DATA/SOLAR_LIKE/kplr003429205_8_COR_PSD_filt_inp.fits')
psd10                 =   np.transpose(hdul10[0].data)
frequency_Hz10       =   copy.deepcopy(psd10[0])
freq_muHz10           =   frequency_Hz10*1e6
power10               =   copy.deepcopy(psd10[1])
smoothed_power10=np.array([np.mean(power10[ii-20:ii+20]) for ii in range(len(power10))])

hdul11=fits.open('/Users/lbugnet/DATA/WG1_ALL_SURVEY_SAVITA/RESULTS_KADACS_COARSE_CheckSTATUS_filt_polfitseg960.000_20.0000d_ppm0_inpaint20/LC_CORR_FILT_INP/kplr008298090_1397_COR_PSD_filt_inp.fits')
psd11                 =   np.transpose(hdul11[0].data)
frequency_Hz11       =   copy.deepcopy(psd11[0])
freq_muHz11           =   frequency_Hz11*1e6
power11               =   copy.deepcopy(psd11[1])
smoothed_power11=np.array([np.mean(power11[ii-20:ii+20]) for ii in range(len(power11))])

import matplotlib as matplotlib
# colors = ['palegoldenrod','palegreen','aquamarine','mediumturquoise','deepskyblue','dodgerblue','mediumpurple','slateblue', 'indigo', 'navy', 'black']
colors = ['crimson','lightcoral','peachpuff','lemonchiffon','lightgreen','mediumturquoise','deepskyblue','dodgerblue','slateblue', 'indigo', 'navy', 'black']
plt.figure(figsize=(10,5))
ax = plt.subplot(111)
# rect=matplotlib.patches.Rectangle((7,1), 280, 1e5, color='peachpuff', alpha=0.4)
# plt.gca().add_patch(rect)
plt.loglog(freq_muHz5, smoothed_power5, color=colors[8])
plt.loglog(freq_muHz4, smoothed_power4, color=colors[7])
plt.loglog(freq_muHz, smoothed_power, color=colors[6])
plt.loglog(freq_muHz3, smoothed_power3, color=colors[5])
plt.plot([283.4,283.4], [1e-1, 1e6], '--', color='gray')

# plt.loglog(freq_muHz2, smoothed_power2, color=colors[4])
plt.loglog(freq_muHz6, smoothed_power6, color=colors[4])
plt.loglog(freq_muHz10, smoothed_power10, color=colors[3])
plt.loglog(freq_muHz8, smoothed_power8, color=colors[2])
plt.loglog(freq_muHz7, smoothed_power7, color=colors[1])
plt.loglog(freq_muHz9, smoothed_power9, color=colors[0])
# plt.loglog(freq_muHz11, smoothed_power11, color=colors[9])


# ax.xaxis.set_ticks_position('both')
# ax.yaxis.set_ticks_position('both')
# ax.tick_params(direction="in")

plt.ylim(0.5, 2e5)
# plt.ylim(-10, 3000)
# plt.xlim(1, 277)
plt.ylabel(r'PSD [ppm$^2$/$\mu$Hz]')
plt.xlabel(r'Fréquence [$\mu$Hz]')
# plt.text(10, 30000,'KIC 1294385',
# plt.text(200, 2000,'KIC 1294385',
        # horizontalalignment='center')#('KIC 1294385', x=0.9, y=0.9)
plt.savefig('/Users/lbugnet/Documents/THESE/FIGS/PSD_evol.png')
