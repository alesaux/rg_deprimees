import numpy as np
from astropy.io import fits
from matplotlib.pyplot import ion
import matplotlib.pyplot as plt
import copy

#ion()

PATH='/Users/students/Arthur/depressed_RG/'

'''----------------------------------------------------'''
''' Reconstruct background from Rafa's fits'''
'''----------------------------------------------------'''

'''----------------------------------------- HARVEY'''
def harvey(param,freq_muHz):
    # print(param)
    A = np.exp(param[0])
    G = np.exp(param[1])*0.5
    alpha = np.exp(param[2])
    fac=np.sqrt(2**(1/alpha)-1)
    den = 1./((fac*freq_muHz/G)**alpha+1.)
    res = A*den
    # print('harvay', res)

    # A = exp(param[0])
    # G = exp(param[1])*0.5D0
    # alpha = exp(param[2])
    # fac=sqrt(2D0^(1D0/alpha)-1D0)
    # den = 1.D0/((fac*X/G)^alpha+1.D0)
    return res

def harvey_bis(param, freq_muHz):
    sig2 = np.exp(2*param[0])
    p2tau = 2*np.pi*np.exp(param[1])
    alpha = 1+np.exp(param[2])

    pi_al=np.pi/alpha
    zetanorm=np.sin(pi_al)/pi_al

    den = 1/((p2tau*freq_muHz)**alpha+1.)

    res = zetanorm*sig2*p2tau*den
    return res
'''----------------------------------------- BACKGROUND'''

# define the function blocks
def white(param, res, freq_muHz):
    B=np.exp(param[0])
    res=res+B
    return res

def Harvey1(param, res, freq_muHz):
    par=param[1:4]
    res=res+harvey_bis(par,freq_muHz)
    #plt.loglog(freq_muHz,harvey_bis(par,freq_muHz))

    return res

def Harvey2(param, res, freq_muHz):
    # print(param[4:7])
    par=param[4:7]
    #print("param", param)#, param[4:7])
    res=res+harvey_bis(par,freq_muHz)
    #plt.loglog(freq_muHz,harvey_bis(par,freq_muHz))
    return res

def gaussian_modes(param, res, freq_muHz):
    print(param)
    A0 = np.exp(param[7])
    x0 = param[8]
    sig0= np.exp(param[9])
    print('res',res)
    var1=(((freq_muHz-x0)/sig0)**2)#<150 #<150 to avoid underflow
    res=res + A0*np.exp(-0.5*var1)
    #plt.loglog(freq_muHz,A0*np.exp(-0.5*var1))
    return res

def gaussian_modes_only(param, freq_muHz):
    print(param)
    A0 = np.exp(param[0])
    x0 = param[1]
    sig0= np.exp(param[2])
    var1=(((freq_muHz-x0)/sig0)**2)#<150 #<150 to avoid underflow
    res=A0*np.exp(-0.5*var1)
    #plt.loglog(freq_muHz,A0*np.exp(-0.5*var1))
    return res

def fct_bg(param,freq_muHz):
    nx=len(freq_muHz)
    res=np.zeros((nx))
    options = {0 : white,
           1 : Harvey1,
           2 : Harvey2,
           3 : gaussian_modes}
    for ic in range(0,4):
        res = options[ic](param, res, freq_muHz)
    return res

def construct_bg(freq_muHz, fit_obj):
    npar=10
    param=np.zeros((npar))
    nx=len(freq_muHz)
    i0=0
    if fit_obj['reswhite']!=None:
        param[i0]=fit_obj['reswhite']
        # print(param)
        i0+=1
    if fit_obj['resharvey1'] !=None :
        # print(i0)

        param[i0:i0+3]=fit_obj['resharvey1']
        # print(param)
        i0+=3
    if fit_obj['resharvey2'] !=None:
        # print(i0, i0+3])
        param[i0:i0+3]=fit_obj['resharvey2']
        i0+=3
        # print(param)
    if fit_obj['resmode'] !=None:
        # print(i0, i0+3])
        print(param)
        param[i0:i0+3]=fit_obj['resmode']
        print(param)
        i0+=3
    bg=fct_bg(param,freq_muHz)

    return bg

# '''----------------------------------------------------'''
# '''CALLING SEQUENCE'''
# '''----------------------------------------------------'''
#
'''restore fiting'''
# hdul = fits.open(PATH+'FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/012405535/back_012405535.fits')
# hdul = fits.open(PATH+'FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/011723893/back_011723893.fits')
# hdul = fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/006975038/back_006975038.fits')
# data=hdul[0].data
# hdul_harvey_corrected=fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/006975038/back_006975038_corrected.fits') #from harveyb .sav rafa fits !
# data_corrected=hdul_harvey_corrected[0].data # GOOD_FIT for Harvey profiles ! correspond to resharveyb to be used inside harvey_bis !

#hdul                =   fits.open('/Users/lbugnet/WORK/FIT_MODES/kplr006975038_2319_COR_PSD_filt_inp.fits')#/Volumes/LaCie/KEPLER_LC/KEPLER_ALL_DR25/RESULTS_KADACS_COARSE_CheckSTATUS_filt_polfitseg960.000_20.0000d_ppm0_inpaint20/LC_CORR_FILT_INP/kplr012008916_19_COR_PSD_filt_inp.fits')
#psd                 =   np.transpose(hdul[0].data)
# largeSep            =   10.61*1e-6
# qCoupling           =   0.35    #effect:change the position of x axis in the echelle diagram
# truePeriodSpacing   =   57.9#sec
# PureL1PressureMode  =   [135*1e-6]#CAREFULL VERY SENSITIVE FOR CENTRAL MODES !!!!!!
# #[171e-6-2*13.68*1e-6,173e-6-13.68*1e-6, 175e-6, 176e-6+13.68*1e-6,175e-6+2*13.68*1e-6]#[170e-6-13.68*1e-6,170*1e-6, 170e-6+13.68*1e-6,170e-6+2*13.68*1e-6,170e-6+3*13.68*1e-6]
# eps_g               =   0.200 # Mosser IV to refine fig 12 #effect:change the position of x axis in the echelle diagram
# # PureL1GravityModes  =   [1/((-ng+eps_g)*truePeriodSpacing) for ng in range(-100, -40)]#range(-210, -125)]
# numax               =   128.4 *1e-6
#frequency_Hz        =   copy.deepcopy(psd[0])
#freq_muHz        =   frequency_Hz*1e6
#power               =   copy.deepcopy(psd[1])
# drot_p              =   0.02
# drot_g              =   30

'''dividing by background'''
#restore fiting'''
'''
hdul2 = fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/006975038/back_006975038.fits')
data=hdul2[0].data

hdul_harvey_corrected=fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/006975038/back_006975038_corrected.fits') #from harveyb .sav rafa fits !
data_corrected=hdul_harvey_corrected[0].data

fit_obj={
		"reswhite" : np.log(data[0]), #bruit blanc haute freq_muHzuence
        "respower" : None, #loi de puissance a basse freq_muHzuence
		"respmodeamp" : np.log(data[1]), #amplitude des modes
		"respmodefreq_muHz" : data[2],#freq_muHzuence des modes
		"respmodewidth" : np.log(data[3]), #largeur des modes
		"harveyaamp" : (data_corrected[0][0]),
		"harveyafreq_Hz" : (data_corrected[0][1]),
		"harveyawidth" : (data_corrected[0][2]),
		"harveybamp" : (data_corrected[1][0]),
		"harveybfreq_Hz" : (data_corrected[1][1]),
		"harveybwidth" : (data_corrected[1][2]),
        "resharvey1" : [ (data_corrected[0][0]),  (data_corrected[0][1]),  (data_corrected[0][2])],
        "resharvey2" : [ (data_corrected[1][0]),  (data_corrected[1][1]),  (data_corrected[1][2])],
        "resmode" : [ np.log(data[1]), data[2], np.log(data[3])]}
		# "harveyaamp" : np.log(data[4]),
		# "harveyafreq_muHz" : np.log(data[5]),
		# "harveyawidth" : np.log(data[6]),
		# "harveybamp" : np.log(data[7]),
		# "harveybfreq_muHz" : np.log(data[8]),
		# "harveybwidth" : np.log(data[9]),
        # "resharvey1": [5.3787350734248518, -5.2089990250331093, 1.0986122886681098],
        # "resharvey2": [5.2475423734428333, -6.2680008957510784, 1.0986122886681098]}
        # # "resharvey1" : [np.log(data[4]), np.log(data[5]), np.log(data[6])],
        # # "resharvey2" : [np.log(data[7]), np.log(data[8]), np.log(data[9])]
	# }
'''
# # '''restore PSD'''
# hdul=fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/006975038/PSD_006975038.fits')
# # hdul = fits.open(PATH+'FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/011723893/PSD_011723893.fits')
# data=hdul[0].data
# freq_muHz=np.transpose(data)[0]*1e6
# power=np.transpose(data)[1]
#
# '''construct background'''
#bg=construct_bg(frequency_Hz*1e6, fit_obj)
#aa=construct_bg(freq_muHz, fit_obj) #careful you should give the freq_muHzuency in muHz !
# SNR=power/bg

#smoothed_power=np.array([np.mean(power[ii-2:ii+2]) for ii in range(len(power))])
#plt.figure()
#plt.loglog(frequency_Hz*1e6, power)
#plt.plot(frequency_Hz*1e6, smoothed_power)
#plt.loglog(frequency_Hz*1e6, bg)

#
# plt.figure()
# plt.loglog(freq_muHz, power, alpha=0.8)
# plt.loglog(freq_muHz, aa)
# plt.ylabel('PSD')
# plt.xlabel('muHz')
#
# '''check background value'''
# # print(np.mean(power[15000:]/aa[15000:])) #should be 1
# # print(np.std(power[15000:]/aa[15000:])) #should be 1
# plt.figure()
# plt.plot(power/aa)
# plt.plot([0,35000], [8,8])
# plt.plot([0,35000], [1,1])
# plt.figure()
# plt.hist(power[15000:]/aa[15000:]/len(aa[15000:]), 1000)
