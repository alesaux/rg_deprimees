import pandas as pd
import matplotlib.pyplot as plt
data2D=pd.read_csv('image_tsne_projections_cercle_gauche.csv')
import numpy as np

KIC_list=data2D['image'].str[14:-14]
for ii,val in enumerate(KIC_list):
    if len(val)==7:
        KIC_list[ii]='0'+KIC_list[ii]
data_clas = pd.read_csv('/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/classification_manually_numax_80_150_Arthur.txt', sep=" ", header=None)
data_clas.columns = ["path", "type"]
KIC_clas=data_clas['path'].str[16:-15]
type_clas=data_clas['type']

# Function to look for matching KIC ID between two files
match = lambda a, b:[[a.index(x),b.index(x)] if (x in b and x in a) else [None,None] for x in a]

index_common_KIC = match((KIC_list.tolist()),(KIC_clas.tolist()))

KIC_common = [KIC_list[x] for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None]
indices_common = [pos for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None]
type_clas_common  =  [type_clas[x] for pos, x in enumerate([row[1] for row in index_common_KIC]) if x is not None]
data2common=data2D.loc[indices_common]
data2common["type"]=type_clas_common
data2common_depressed=data2common[data2common['type']==1]
data2common_normal=data2common[data2common['type']==0]
data2common_normal2=data2common[data2common['type']==2]
data2common_normal3=data2common[data2common['type']==2]
data2common_normal4=data2common[data2common['type']==2]
data2common_normal5=data2common[data2common['type']==2]
data2common_normal6=data2common[data2common['type']==2]
# data2common = data2D[pos for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None]
# data2common['type'] = type_clas_common
plt.ion()
plt.figure()
plt.plot(data2D['coordx'], data2D['coordy'], '.', color='dodgerblue')
plt.plot(data2common_normal['coordx'], data2common_normal['coordy'], '.', color='green')
plt.plot(data2common_depressed['coordx'], data2common_depressed['coordy'], '.', color='red')
plt.plot(data2common_normal2['coordx'], data2common_normal2['coordy'], '.', color='orange')
# plt.plot(data2common_normal3['coordx'], data2common_normal3['coordy'], '.', color='orange')
# plt.plot(data2common_normal4['coordx'], data2common_normal4['coordy'], '.')
# plt.plot(data2common_normal5['coordx'], data2common_normal5['coordy'], '.')
# plt.plot(data2common_normal6['coordx'], data2common_normal6['coordy'], '.')



'''---------------------3D----------------------'''

import pandas as pd
import matplotlib.pyplot as plt
data2D=pd.read_csv('image_tsne_projections_cercle_gauche_3D.csv')
import numpy as np

KIC_list=data2D['image'].str[14:-14]
for ii,val in enumerate(KIC_list):
    if len(val)==7:
        KIC_list[ii]='0'+KIC_list[ii]
data_clas = pd.read_csv('/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/classification_manually_numax_80_150_Arthur.txt', sep=" ", header=None)
data_clas.columns = ["path", "type"]
KIC_clas=data_clas['path'].str[16:-15]
type_clas=data_clas['type']

# Function to look for matching KIC ID between two files
match = lambda a, b:[[a.index(x),b.index(x)] if (x in b and x in a) else [None,None] for x in a]

index_common_KIC = match((KIC_list.tolist()),(KIC_clas.tolist()))

KIC_common = [KIC_list[x] for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None]
indices_common = [pos for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None]
type_clas_common  =  [type_clas[x] for pos, x in enumerate([row[1] for row in index_common_KIC]) if x is not None]
data2common=data2D.loc[indices_common]
data2common["type"]=type_clas_common
data2common_depressed=data2common[data2common['type']==1]
data2common_normal=data2common[data2common['type']==0]
data2common_normal2=data2common[data2common['type']==2]
data2common_normal3=data2common[data2common['type']==2]
data2common_normal4=data2common[data2common['type']==2]
data2common_normal5=data2common[data2common['type']==2]
data2common_normal6=data2common[data2common['type']==2]
# data2common = data2D[pos for pos, x in enumerate([row[0] for row in index_common_KIC]) if x is not None]
# data2common['type'] = type_clas_common
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D

# plt.ion()
fig=plt.figure()

# ax = plt.axes(projection='3d')
ax1 = fig.add_subplot(111,projection='3d')


ax1.scatter(data2D['coordx'], data2D['coordy'], data2D['coordz'], color='dodgerblue', alpha=0.2)

# plt.plot(data2D['coordx'], data2D['coordy'], '.')
# plt.plot(data2common_normal['coordx'], data2common_normal['coordy'], '.')
# plt.figure()
#
# ax = plt.axes(projection='3d')
ax1.scatter(data2common_depressed['coordx'], data2common_depressed['coordy'],data2common_depressed['coordz'], color='red', s=50, alpha=1)
ax1.scatter(data2common_normal['coordx'], data2common_normal['coordy'],data2common_normal['coordz'], color='green', s=50, alpha=1)

plt.show()# plt.plot(data2common_normal2['coordx'], data2common_normal2['coordy'], '.')
# plt.plot(data2common_normal3['coordx'], data2common_normal3['coordy'], '.')
# plt.plot(data2common_normal4['coordx'], data2common_normal4['coordy'], '.')
# plt.plot(data2common_normal5['coordx'], data2common_normal5['coordy'], '.')
# plt.plot(data2common_normal6['coordx'], data2common_normal6['coordy'], '.')
