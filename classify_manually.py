import numpy as np
import glob
import matplotlib.image as mpimg
from matplotlib.pyplot import ion
import os

ion()
directory='/Users/lbugnet/DATA/DEPRESSED/VRARD_12/'
array=[]
'''import images HERE'''

for image_path in glob.glob(directory+"*.png"):
    path, filename = os.path.split(image_path)
    img=mpimg.imread(image_path)
    imgplot = plt.imshow(img)

    type = input('0: normal star'+'\n'+ '1 : depressed star' + '\n' +'2: droopy-like' + '\n'+ '3 : l=2 depressed' + '\n' + '4: wrong numax' + '\n' + '5 : wrong dnu' + '\n' + '6 : other' )
    array += [[filename[:-4],type]]

np.savetxt(directory+"classification_manually.txt",np.c_[array], delimiter=" ", newline="\n", fmt="%s")
