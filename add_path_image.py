import numpy as np
import csv


#PATH_SAVE = '/Users/alesaux/rg_deprimees/alesaux-rg_deprimees-9d8f5a73088e/'
PATH_SAVE = '/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/'

with open('/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/image_tsne_projections_cercle_gauche.csv','r') as csvfile :
  data_graph = csv.reader(csvfile, dialect='excel', delimiter=',')

  next(data_graph)

  path_image, x, y,z, D = [], [], [], [],[]

  for row in data_graph:
    while '' in row:
      row.remove('')
    path_image += [[i for i in row][0]]
    x += [[i for i in row][1]]
    y += [[i for i in row][2]]
    #z += [[i for i in row][3]]
    D += [[i for i in row][3]]

path_image = ['"<img src = https://bitbucket.org/alesaux/rg_deprimees/raw/c0bc1df6559f9680e872cf1adf4dbf7aa4030716/echelle_diag_depressed_select_cercle_gauche/'+str(i[1:-1])+' />"' for i in path_image]

np.savetxt(PATH_SAVE+"image_tsne_projections_cercle_gauche_with_path.csv", np.c_[path_image,x,y,D],\
  delimiter = ",", fmt=["%-s","%-s","%-s","%-s"], header = "image,coordx,coordy,D",comments="")
