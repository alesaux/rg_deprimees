# Gaussian fit
import numpy as np


def gaussian_f(f, numax, deltanu, A):
    ## numax = mean value, deltanu = standard deviation, A = scale factor
    exp_numerator = -(f - numax)**2
    exp_denominator = 2 * deltanu**2
    G = A * np.exp(exp_numerator / exp_denominator)
    return G

def fit_gaussian(freq_muHz,numax,sigma, A0):
    var1=(((freq_muHz-numax)/sigma)**2)#<150 #<150 to avoid underflow
    res= A0*np.exp(-0.5*var1)
    #plt.loglog(freq_muHz,A0*np.exp(-0.5*var1))
    return res
