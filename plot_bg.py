'''pour avoir juste les  harvey !!!!!'''
# from construct_bg import *
'''pour avoir toutes les composante !!!!!'''
from construct_bg_with_gaussian import *
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
import copy  as copy
# '''FALSE CALLING !!!!!!!! SEE constrcut_bg_with_gaussian de FIT_MODES '''



plt.ion()
'''restore fiting'''
hdul2 = fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/001294385/back_001294385.fits')
# hdul2 = fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/001294385/back_001294385.fits')
data=hdul2[0].data
hdul_harvey_corrected = fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/001294385/back_001294385_corrected.fits')

# hdul_harvey_corrected=fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/001294385/back_001294385_corrected.fits') #from harveyb .sav rafa fits !
data_corrected=hdul_harvey_corrected[0].data

fit_obj={
		"reswhite" : np.log(data[0]), #bruit blanc haute freq_muHzuence
        "respower" : None, #loi de puissance a basse freq_muHzuence
		"respmodeamp" : np.log(data[1]), #amplitude des modes
		"respmodefreq_muHz" : data[2],#freq_muHzuence des modes
		"respmodewidth" : np.log(data[3]), #largeur des modes
		"harveyaamp" : (data_corrected[0][0]),
		"harveyafreq_Hz" : (data_corrected[0][1]),
		"harveyawidth" : (data_corrected[0][2]),
		"harveybamp" : (data_corrected[1][0]),
		"harveybfreq_Hz" : (data_corrected[1][1]),
		"harveybwidth" : (data_corrected[1][2]),
        "resharvey1" : [ (data_corrected[0][0]),  (data_corrected[0][1]),  (data_corrected[0][2])],
        "resharvey2" : [ (data_corrected[1][0]),  (data_corrected[1][1]),  (data_corrected[1][2])],
        "resmode" : [ np.log(data[1]), data[2], np.log(data[3])]}
		# "harveyaamp" : np.log(data[4]),
		# "harveyafreq_muHz" : np.log(data[5]),
		# "harveyawidth" : np.log(data[6]),
		# "harveybamp" : np.log(data[7]),
		# "harveybfreq_muHz" : np.log(data[8]),
		# "harveybwidth" : np.log(data[9]),
        # "resharvey1": [5.3787350734248518, -5.2089990250331093, 1.0986122886681098],
        # "resharvey2": [5.2475423734428333, -6.2680008957510784, 1.0986122886681098]}
        # # "resharvey1" : [np.log(data[4]), np.log(data[5]), np.log(data[6])],
        # # "resharvey2" : [np.log(data[7]), np.log(data[8]), np.log(data[9])]
	# }
# # '''restore PSD'''
hdul=fits.open('/Users/lbugnet/DATA/RG_catalog/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/001294385/PSD_001294385.fits')
# hdul=fits.open('/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/001294385/PSD_001294385.fits')
# # hdul = fits.open(PATH+'FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/011723893/PSD_011723893.fits')
# data=hdul[0].data
# freq_muHz=np.transpose(data)[0]*1e6
# power=np.transpose(data)[1]
#
# '''construct background'''
psd                 =   np.transpose(hdul[0].data)
frequency_Hz        =   copy.deepcopy(psd[0])
freq_muHz           =   frequency_Hz*1e6
power               =   copy.deepcopy(psd[1])

bg=construct_bg(frequency_Hz*1e6, fit_obj)
aa=construct_bg(freq_muHz, fit_obj) #careful you should give the freq_muHzuency in muHz !
# SNR=power/bg

smoothed_power=np.array([np.mean(power[ii-5:ii+5]) for ii in range(len(power))])


# freq_muHz=np.transpose(data)[0]*1e6
# power=np.transpose(data)[1]
#construct background
# aa=construct_bg(freq_muHz, fit_obj) #careful you should give the freq_muHzuency in muHz !
#
# plt.plot(freq_muHz, smoothed_power, color='darkgray')
# plt.plot(freq_muHz, aa, color='dodgerblue')
# plt.plot(freq_muHz,np.ones(len(freq_muHz))*fit_obj['reswhite'], '.-', color='darkgray')
# plt.plot(freq_muHz,harvey_bis(fit_obj['resharvey1'],freq_muHz), '--', color='black')
# plt.plot(freq_muHz,harvey_bis(fit_obj['resharvey2'],freq_muHz), '--', color='crimson')
# # plt.ylim(1, 1e5)
# plt.ylim(-10, 3000)
# plt.xlim(1, 277)
# plt.ylabel(r'PSD [ppm$^2$/$\mu$Hz]')
# plt.xlabel(r'Frequency [$\mu$Hz]')
# # plt.text(10, 30000,'KIC 1294385',
# plt.text(200, 2000,'KIC 1294385',
#         horizontalalignment='center')#('KIC 1294385', x=0.9, y=0.9)
# plt.savefig('/Users/lbugnet/Documents/THESE/FIGS/granulation_harvey_nolog.png')

import matplotlib as matplotlib
plt.figure()
# rect=matplotlib.patches.Rectangle((7,1), 280, 1e5, color='peachpuff', alpha=0.4)
# plt.gca().add_patch(rect)
plt.loglog(freq_muHz, smoothed_power, color='darkgray')
plt.loglog(freq_muHz, aa, color='dodgerblue')
plt.loglog(freq_muHz,gaussian_modes_only(fit_obj['resmode'],freq_muHz), '--', color='lightsalmon')
plt.loglog(freq_muHz,np.ones(len(freq_muHz))*fit_obj['reswhite'], '.-', color='darkgray')
plt.loglog(freq_muHz,harvey_bis(fit_obj['resharvey1'],freq_muHz), '--', color='black')
plt.loglog(freq_muHz,harvey_bis(fit_obj['resharvey2'],freq_muHz), '--', color='crimson')
plt.ylim(1, 1e5)
# plt.ylim(-10, 3000)
plt.xlim(1, 277)
plt.ylabel(r'PSD [ppm$^2$/$\mu$Hz]')
plt.xlabel(r'Fréquence [$\mu$Hz]')
# plt.text(10, 30000,'KIC 1294385',
# plt.text(200, 2000,'KIC 1294385',
        # horizontalalignment='center')#('KIC 1294385', x=0.9, y=0.9)
plt.savefig('/Users/lbugnet/Documents/THESE/FIGS/components_in_spectrum.png')
