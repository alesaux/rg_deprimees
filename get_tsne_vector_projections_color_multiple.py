from sklearn.manifold import TSNE
import numpy as np
import glob, json, os
import csv

# create datastores
vector_files = []
image_vectors = []
chart_data = []
maximum_imgs = 15000

# build a list of image vectors
vector_files = glob.glob('image_vectors/*.npz')[:maximum_imgs]
for c, i in enumerate(vector_files):
  image_vectors.append(np.loadtxt(i))
  print(' * loaded', c, 'of', len(vector_files), 'image vectors')

# build the tsne model on the image vectors
print('building tsne model')
model = TSNE(n_components=3, random_state=0)
np.set_printoptions(suppress=True)
fit_model = model.fit_transform( np.array(image_vectors) )

# store the coordinates of each image in the chart data
for c, i in enumerate(fit_model):
  D = 'Unknown'
  print(vector_files[c])
  print(vector_files[c][-9])
  if vector_files[c][-9] == '0':
    D = 'Normale'
  if vector_files[c][-9] == '1':
    D = 'Deprimee'
  if vector_files[c][-9] == '2':
    D = 'Droopy'
  if vector_files[c][-9] == '3':
    D = 'l2_depr'
  if vector_files[c][-9] == '4':
    D = 'wrong_numax'
  if vector_files[c][-9] == '5':
    D = 'wrong_deltanu'
  print(D)
  image_name = os.path.basename(vector_files[c])
#  image_name = os.path.basename('KIC:'+str(vector_files[c][26:35]))
  chart_data.append({
    'image': os.path.join('"'+image_name[:-4]+'"'),
    'coordx': np.float64(i[0]),
    'coordy': np.float64(i[1]),
    'coordz': np.float64(i[2]),
    'D': D
  })

with open('image_tsne_projections_cercle_gauche_3D.json', 'w') as out:
  json.dump(chart_data, out)

with open('image_tsne_projections_cercle_gauche_3D.csv', mode='w') as csv_file:
  fieldnames = ['image','coordx', 'coordy','coordz','D']
 # fieldnames = ['coordx', 'coordy','D']
  writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

  writer.writeheader()
  for data in chart_data:
    writer.writerow(data)
