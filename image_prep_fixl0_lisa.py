# Preparation images diagrammes echelle
# avec la position du mode l=0 fixe
from astropy.io import fits
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import ion
import scipy.ndimage as sc
from PSD_PATH import *
import csv
from construct_bg import construct_bg
from gaussian_fit import gaussian_f, fit_gaussian
from skimage.transform import rescale, resize, downscale_local_mean
import copy
import pandas as pd
from tqdm import tqdm
#plt.ion()
'''PATHS'''
# # Repertory with kepler data
# PATH_DATA_LISA  =   '/Volumes/Lisa/KEPLER_ALL_DR25/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/'
# PATH_DATA_ARTHUR    =   '/Volumes/Data_3To/Arthur/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/'
#repertory     ='/Volumes/Data_3To/Arthur/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/'
#repertory_old ='/Volumes/Data_3To/Arthur/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS_OLD/'
#repertory_l0  ='/Users/students/Arthur/depressed_RG/LISA_TEST_L1_DEPRESSED/FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/'
#repertory_l0  ='/Volumes/Data_3To/DATA/KEPLER/KEPLER_LC/RG_CATALOG/RG_DR25/TEST_GOOD_FIT_TASSOUL_SM/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2/RESULTS_FITS/'
#repertory_l0  ='/Volumes/Data_3To/DATA/KEPLER/KEPLER_LC/RG_CATALOG/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/'
repertory_l0  ='/Users/lbugnet/DATA/RG_catalog/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS/'
#repertory_l0 = '/Volumes/Data_3To/DATA/KEPLER/KEPLER_LC/RG_CATALOG/BUNDLE_RG/FULL_RG_CATALOG/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_multi/RESULTS_FITS/'
repertory_noBG  ='/Users/lbugnet/DATA/RG_catalog/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n2_June2019/RESULTS_FITS_NO_BGcorrected/'

# Create a path for every fits file containing PSD
path_data         = PSD_PATH_IN_REPERTORY(repertory_l0)
path_datab        = BACK_PATH_IN_REPERTORY(repertory_noBG)
path_data_correct = BACK_CORRECT_PATH_IN_REPERTORY(repertory_l0)
path_data_l0      = L0_PATH_IN_REPERTORY(repertory_l0)
#path_datab2        = BACK_PATH_IN_REPERTORY(repertory)


# seuil background
bg = 8

# Path to file saving directory

#PATH_SAVE1 = '/Users/alesaux/CEA/Depressed_RG/data/echelle_diag_'+str(bg)+'bg_fixl0_Yu2018_numaxRafa_newGaussian/'
#PATH_SAVE2 = '/Users/alesaux/CEA/Depressed_RG/data/spectre_echelle_'+str(bg)+'bg_fixl0_Yu2018_numaxRafa_newGaussian/'

PATH_ARTHUR='/Users/students/Arthur/depressed_RG/'
PATH_LISA='/Users/lbugnet/WORK/ARTHUR_ML/'
PATH_SAVE_LISA1='/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/fig_echelle/'
PATH_SAVE_LISA2='/Users/lbugnet/WORK/ARTHUR_ML/rg_deprimees/fig_echelle_and_spectrum/'

PATH_SAVE1 = PATH_ARTHUR+'data/echelle_diag_'+str(bg)+'bg_fixl0_Yu2018_interpolation_numaxRafa_ALL/'
PATH_SAVE2 = PATH_ARTHUR+'data/spectre_echelle_'+str(bg)+'bg_fixl0_Yu2018_interpolation_ALL/'

#PATH_SAVE = '/Users/students/Arthur/depressed_RG/'


'''Open file containing numax, delta nu (Yu 2018)'''
#with open('/Users/alesaux/CEA/Depressed_RG/RG_Kepler_Yu2018.csv','r') as txtfile :
#with pandas:
df=pd.read_csv('/Users/lbugnet/DATA/RG_catalog/rg_catalog_breton_v6.5.7.csv', index_col=0)
KIC_param=df.index
delta_med=df['Dnu_median'].to_numpy()
numax_med=df['numax_median'].to_numpy()
numaxYu=numax_med
deltanuYu=delta_med
# with open('/Users/lbugnet/DATA/RG_catalog/rg_catalog_breton_v6.5.7.csv', 'r') as txtfile :
#     extra_param = csv.reader(txtfile, dialect='excel',delimiter=',')
#
#     next(extra_param)
#
#     KIC_param, delta_med, numax_med = [], [], []
#     # KIC_param, numaxYu, deltanuYu = [], [], []
#
#     for row in extra_param:
#         while '' in row:
#             row.remove('')
#         KIC_param += [[np.float(i) for i in row][0]]
#         delta_med += [[np.float(i) for i in row][37]]
#         numax_med += [[np.float(i) for i in row][38]]

#with open(PATH_LISA+'RG_Kepler_Yu2018.csv','r') as txtfile :
    # extra_param = csv.reader(txtfile, dialect='excel',delimiter=',')
    #
    # next(extra_param)
    #
    # KIC_param, numaxYu, deltanuYu , amplYu= [], [], [], []
    #
    # for row in extra_param:
    #     while '' in row:
    #         row.remove('')
    #     KIC_param += [[np.float(i) for i in row][0]]
    #     deltanuYu += [[np.float(i) for i in row][5]]
    #     numaxYu += [[np.float(i) for i in row][4]]
    #     amplYu += [[np.float(i) for i in row][6]]

KIC_param = [np.int(i) for i in KIC_param]

KICl0, all_freq_l0 = [], []
for ii in tqdm(range(3880,len(path_data_l0))):
    freq_l0 = []
    KICl0.append(np.float(path_data_l0[ii][0:9]))
    pkb_file = repertory_l0+path_data_l0[ii]

  # Open file containing numax, delta nu (Yu 2018)
    with open(pkb_file,'r') as filename :
        data_l0 = csv.reader(filename, dialect='excel',delimiter=' ')

        for i in range(4):
            next(data_l0)

        for row in data_l0:
            while '' in row:
                row.remove('')
            if np.float(row[1]) == 0.0:
                freq_l0 += [[np.float(i) for i in row][2]]

    all_freq_l0.append(freq_l0)

print('length KICl0 ',len(KICl0))


# Function to look for matching KIC ID between two files
match = lambda a, b:[[a.index(x),b.index(x)] if (x in b and x in a) else [None,None] for x in a]

KICtab, numax, deltanu, freqtab, powertab = [], [], [], [], []
numaxtab, deltanutab = [], []

# Loop to extract data from each file
#for ii in range(18697,len(path_data),1) :
for ii in tqdm(range(len(KIC_param))):#range(0,30,4) :
#for ii in range(1,3001,1000):

  KIC_tmp, KIC_index, freq, power = [], [], [], []

  path_fits = repertory_l0+path_data[ii]
  #print(path_data[ii])
  #print(path_data[ii][14:23])
  #path_fits = '/Users/students/Arthur/depressed_RG/012405535/kplr012405535_611_COR_PSD_filt_inp.fits'
  hdu = fits.open(path_fits)[0]
  KICtab.append(np.int(path_data[ii][14:23]))
  KIC = np.int(path_data[ii][14:23])
  KIC_tmp.append(np.int(path_data[ii][14:23]))
  #KICtab.append(012405535.0)
  data = np.array(hdu.data)
  freq = data[:,0]*1e6                    #convert to muHz
  power = data[:,1]

  powertab.append(power)
  freqtab.append(freq)

  path_back = repertory_noBG+path_datab[ii]
  #path_back = '/Users/students/Arthur/depressed_RG/012405535/back_012405535.fits'
  hdul = fits.open(path_back)[0]
  data_back=hdul.data
  #print(hdul.header)

  path_back_correct = repertory_l0+path_data_correct[ii]
  #path_back = '/Users/students/Arthur/depressed_RG/012405535/back_012405535.fits'
  hduc = fits.open(path_back_correct)[0]
  data_back_correct = hduc.data
  #print(hdul.header)

  #print('KIC', KIC)

  if KICl0.count(KIC) == 0:
    #print('=0')
    continue

  freq_l0 = all_freq_l0[KICl0.index(KIC)]

  KIC_index = match(KIC_tmp,KIC_param)

  if KIC_index[0][1] == None:
    print('KIC not in Yu2018')
    continue
    print('ok')


  fit_obj={
      "reswhite" : np.log(data_back[0]), #bruit blanc haute freq_muHzuence
      "respower" : None, #loi de puissance a basse freq_muHzuence
      "respmodeamp" : np.log(data_back[1]), #amplitude des modes
      #"respmodeamp" : np.log(amplYu[KIC_index[0][1]]),
      "respmodefreq_muHz" : data_back[2],#freq_muHzuence des modes
      #"respmodefreq_muHz" : np.log(numaxYu[KIC_index[0][1]]),
      "respmodewidth" : np.log(data_back[3]), #largeur des modes
      #"respmodewidth" : np.log(deltanuYu[KIC_index[0][1]]),
      #"resmode" : [ np.log(data_back[1]), data_back[2], np.log(data_back[3])],
      "harveyaamp" : data_back_correct[0][0],
      "harveyafreq_muHz" : data_back_correct[0][1],
      "harveyawidth" : data_back_correct[0][2],
      "harveybamp" : data_back_correct[1][0],
      "harveybfreq_muHz" : data_back_correct[1][1],
      "harveybwidth" : data_back_correct[1][2],
      "resharvey1" : [data_back_correct[0][0], data_back_correct[0][1], data_back_correct[0][2]],
      "resharvey2" : [data_back_correct[1][0], data_back_correct[1][1], data_back_correct[1][2]]
    }


  numax = numaxYu[KIC_index[0][1]]
  deltanu = deltanuYu[KIC_index[0][1]]
  #A = amplYu[KIC_index[0][1]]

  #numax = data_back[2]
  #deltanu = data_back[3]
  A = data_back[1]

  diff_numax_l0 = []

  for i in freq_l0:
    diff_numax_l0.append(np.abs(i - numax))

  index_diff = diff_numax_l0.index(np.min(diff_numax_l0))


  l0 = freq_l0[index_diff]

  if deltanuYu[KIC_index[0][1]] == 0.0:
    print('delta nu = 0')
    print(deltanuYu[KIC_index[0][1]])
    continue


  fbin    = np.mean(np.diff(freq))      #frequencies in each bin
  #deltanu = np.float(data_back[3])
  #print('deltanu',deltanu)
  deltanu = round(deltanu/fbin)*fbin
  #print('deltanu aft',deltanu)
  #numax   = np.float(data_back[2])
  numax   = round(numax/(fbin))*(fbin)

  numaxtab.append(numax)
  deltanutab.append(deltanu)

  '''--------------------------------------'''

  #power3nu = power[np.where((freq>=(numax-3.5*deltanu)) & (freq <= (numax+3.5*deltanu)))]
  #freq3nu = freq[np.where((freq>=(numax-3.5*deltanu)) & (freq <= (numax+3.5*deltanu)))]

  power3nu = power[np.where((freq>=(l0-3.3*deltanu)) & (freq <= (l0+3.7*deltanu)))]
  freq3nu = freq[np.where((freq>=(l0-3.3*deltanu)) & (freq <= (l0+3.7*deltanu)))]


  #print('P3',len(power3nu))
  #print('F3',len(freq3nu))

  #print(power3nu)

  '''
  plt.figure(1)
  plt.plot(np.log10(freq3nu[0]),np.log10(power3nu[0]),'-')
  plt.xlabel(r'Frequency ($\mu$Hz)')
  plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  #plt.show()
  '''

  #for ii in range(0,len(path_data),1) :
  #for ii in range(0,1,1) :
  #  path_back = '/Volumes/Data_3To/Arthur/FITS_20d_OFAC3_good_BACK_100_l1l2_fix_slope_fix_recenternul0c_n4/RESULTS_FITS/012405535/back_012405535.fits'

  '''restor PSD'''
  #hdul = fits.open(PATH+'FITS_20d_OFAC3_TEST_MASK_l1_good_BACK_v1/RESULTS_FITS/012405535/PSD_012405535.fits')
  #data=hdul[0].data
  #freq_muHz=np.transpose(data)[0]*1e6
  #power=np.transpose(data)[1]

  '''construct background'''
  back = construct_bg(freq3nu, fit_obj) #careful you should give the freq_muHzuency in muHz !

  back_all = construct_bg(freq, fit_obj)
  #print(len(back))
  #print(len(power3nu[0]))

  plt.figure(1)
  plt.loglog(freq,power,'-')
  plt.loglog(freq,back_all)
  plt.xlabel(r'Frequency ($\mu$Hz)')
  plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  #plt.show()

  #print('back',back)

  # '''
  # plt.figure(2)
  # plt.loglog(freq3nu, [np.mean(power3nu[jj-2:jj+2]) for jj in range(len(power3nu))])
  # plt.loglog(freq3nu, back)
  # plt.xlabel(r'Frequency ($\mu$Hz)')
  # plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  # #plt.show()
  #
  # plt.figure(12)
  # plt.loglog(freq, power)
  # plt.loglog(freq, back_all)
  # plt.xlabel(r'Frequency ($\mu$Hz)')
  # plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  #
  # plt.figure(3)
  # plt.loglog(freq3nu, power3nu)
  # plt.loglog(freq3nu, back)
  # plt.xlabel(r'Frequency ($\mu$Hz)')
  # plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  # #plt.show()
  # '''
  '''normalisation with background'''
  power3nu_nobg = power3nu/back
  # '''
  # plt.figure(4)
  # plt.loglog(freq3nu, power3nu_nobg)
  # plt.loglog(freq3nu, back)
  # plt.xlabel(r'Frequency ($\mu$Hz)')
  # plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  # #plt.show()
  # '''

  #back_numax = back[np.abs(np.array(freq3nu)-numax).argmin()] #value of background at numax
  FWHM=0.59*numax**(0.9)
  sigma=FWHM/(2*np.sqrt(2*np.log(2))) #l'erreur etait là pour la largeur !!!! maintenant ok
  #fit_gaussien = fit_gaussian(freq3nu,numax,sigma, 8.0e6*numax**(-2.29)/back_numax)
  fit_gaussien = fit_gaussian(freq3nu,numax,sigma, 3.5)

  plt.figure(5)
  plt.loglog(freq3nu, power3nu_nobg,label='spectra')
  plt.loglog(freq3nu, fit_gaussien,label='new')
  plt.xlabel(r'Frequency ($\mu$Hz)')
  plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  plt.legend()

  plt.figure(6)
  plt.loglog(freq3nu, power3nu_nobg*(1.0/fit_gaussien))
  plt.xlabel(r'Frequency ($\mu$Hz)')
  plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  #plt.show()


  '''removing gaussian of the mode to flaten amplitudes'''
  power3nu_nobg_droit = power3nu_nobg*(1.0/fit_gaussien)

  '''--------------------------------------'''
  '''SMOOTHING SPECTRUM (attention a ne pas faire avant l'étude statistique sinon biais !!!!!)'''
  '''--------------------------------------'''
  '''finding signaficative points that will be kept (signal>8xbackground)'''
  indices_not_cut=[]
  for jj in range(len(back)):
       if (np.float(bg) <= power3nu_nobg[jj]) :
           indices_not_cut+=[jj]

  power_smooth=copy.copy(power3nu_nobg_droit)#np.zeros(len(power3nu_nobg_droit))
  for ii,pp in enumerate(indices_not_cut): #we take only values
      if (pp > int(round(deltanu)) and pp<len(power3nu_nobg_droit)-int(round(deltanu))): #on smooth sur deltanu/25 points
          power_smooth[pp]=np.mean(power3nu_nobg_droit[pp-int(round(deltanu)):pp+int(round(deltanu))])
      # elif ii<=5:
          # power_smooth[ii]=np.mean(power[0:ii])
      else:
          power_smooth[pp]=power3nu_nobg_droit[pp]

  power3nu_nobg_droit=copy.copy(power_smooth)


  plt.figure(11)
  plt.plot(freq3nu, power3nu_nobg_droit)
  plt.xlabel(r'Frequency ($\mu$Hz)')
  plt.ylabel(r'PSD (ppm$^2$/$\mu$Hz))')
  #plt.show()

  '''removing non-signaficative points'''
  for jj in range(len(back)):
      if (np.float(bg) >= power3nu_nobg[jj]) :
        power3nu_nobg_droit[jj] = 0.0

  def echelle_diagram(frequency, power, deltanu):
      '''--------------------------------------'''
      '''find number of bins corresponding to dnu'''
      '''--------------------------------------'''
      fbin=np.mean(np.diff(frequency))
      number_of_bin_for_dnu=int(1.0*deltanu/fbin)

      #resize array
      sizeh=int(len(frequency)/number_of_bin_for_dnu)
      #print(sizeh)

      frequency=frequency[:sizeh*number_of_bin_for_dnu]
      power=power[:sizeh*number_of_bin_for_dnu]

      '''--------------------------------------'''
      '''Echelle diagram'''
      '''--------------------------------------'''
      Z = np.reshape(np.transpose(power), ( sizeh,number_of_bin_for_dnu))
      Z = [l.tolist() for l in Z]

      if len(Z) == 0:
        Z = np.zeros(6)
      try:
        Z=np.array(Z)/np.max(Z) #normalise par rapport au max du diagrame
      except ValueError:  #raised if `Z` is empty.
        pass

      return Z

  image_resized=echelle_diagram(freq3nu, power3nu_nobg_droit, deltanu)
  Z=image_resized

  plt.figure(7)
  fig, ax = plt.subplots()
  p = ax.pcolor(image_resized, cmap='binary', vmin=np.min(image_resized), vmax=np.max(image_resized))
  plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
  plt.ylabel('Frequency (muHz)')
  plt.xlabel('Frequency [dnu] (muHz)')


  Z = resize(Z, (128, 128)) #convert to image number of pixels size
  Z_bw=copy.copy(Z)

  for j in range(len(Z[:,0])):
    #Z_bw[j,:]=[1 if k >=0.3 else 0 for k in Z_normalized[j,:]]    #pour images binaires
    #Z_bw[j,:]=[k if k >=0.3 else 0 for k in Z_normalized[j,:]]     # pour images avec seuil et interpolation
    Z_bw[j,:]=[k for k in Z[j,:]]     # pour images sans seuil, le seuil est déjà fait à 8x background avant: faux car on lisse le diagram echelle, on reenlever ici



  #print('de',deltanu)

  '''
  delta_step = np.float("{0:.2f}".format(round(deltanu,2)))
  min_freq = np.float("{0:.2f}".format(round(np.min(freq3nu))))
  max_freq = np.float("{0:.2f}".format(round(np.max(freq3nu))))
  '''

  delta_step = deltanu
  delta_step = np.float("{0:.3f}".format(round(deltanu,3)))

  min_freq = np.int(np.min(freq3nu))
  max_freq = np.int(np.max(freq3nu))

  ylab = []

  for ii in np.arange(min_freq,max_freq,delta_step):
    ylab.append(np.float("{0:.2f}".format(round(ii,2))))

  #print('delta_step', delta_step)


  if len(str(KIC)) == 6 :
    kic = '00'+str(KIC)
  if len(str(KIC)) == 7 :
    kic = '0'+str(KIC)
  if len(str(KIC)) == 8 :
    kic = str(KIC)

  image_resized = copy.copy(Z_bw)
  plt.figure()
  fig, ax = plt.subplots()
  #ax.contourf(image_resized, 10, cmap='RdGy',title='KIC '+kic)
  ax.contourf(image_resized, 200, cmap='binary',title='KIC '+kic)
  ax.get_xaxis().set_visible(False)
  ax.get_yaxis().set_visible(False)
  plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
  plt.savefig(PATH_SAVE_LISA1+'echelle_diag_'+str(KIC)+'_'+str(bg)+'bg_Yu18.png')

  # plt.figure(8)
  # fig, ax = plt.subplots()
  # #plt.rcParams['image.interpolation'] = 'bilinear'
  # #plt.axis('on')
  # p = ax.imshow(image_resized, cmap='binary', vmin=np.min(image_resized), vmax=np.max(image_resized),
  #   interpolation = 'nearest',aspect = 'auto',origin='lower')
  # plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
  # #plt.savefig(PATH_SAVE1+'echelle_diag_'+str(KIC)+'_'+str(bg)+'bg_fixl0_Yu18.png')
  # #plt.show()
  #
  #
  plt.figure(9)
  fig, ax = plt.subplots(nrows=2, ncols=1, sharex=False, sharey=False, figsize=(9,9))
  #plt.axis('on')
  #p = ax[0].imshow(image_resized, cmap='binary', vmin=np.min(image_resized), vmax=np.max(image_resized),
    #interpolation = 'nearest',aspect = 'equal',origin='lower')
  p = ax[0].contourf(image_resized, 200,cmap='binary')
  #plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
  ax[0].set_ylabel(r'Frequency')
  ax[0].set_xlabel(r'Frequency ($\Delta \nu$ ='+str(delta_step)+r'$\mu$Hz)')
  ax[0].get_xaxis().set_visible(False)
  ax[0].get_yaxis().set_visible(False)
  #ax[0].set_yticks(range(6))
  #ax[0].set_yticklabels(ylab)
  #ax[0].set_xticks(range(0,number_of_bin_for_dnu, np.int(number_of_bin_for_dnu/deltanu)))
  #ax[0].set_xticklabels(range(np.int(deltanu)+1))
  #ax[0].get_xaxis().set_visible(False)
  #ax[0].get_yaxis().set_visible(False)
  ax[0].title.set_text('KIC:'+str(KIC)+' | 'r'$\Delta \nu$ = '+str(delta_step)+r' $\mu$Hz')
  ax[1].plot(freq3nu, power3nu_nobg)
  ax[1].set_xlabel(r'Frequency ($\mu$Hz)')
  ax[1].set_ylabel(r'Signal/Background')
  plt.savefig(PATH_SAVE_LISA2+'echelle_diag_and_spectrum_'+str(KIC)+'_'+str(bg)+'bg_Yu18.png')
  #plt.savefig(PATH_SAVE+'echelle_diag_'+str(KIC)+'_'+str(bg)+'bg_Yu18.png')
  '''
  #p = ax[1].pcolor(image_resized, cmap='gray', vmin=np.min(image_resized), vmax=np.max(image_resized))
  p = ax[1].imshow(image_resized, cmap='gray', vmin=np.min(image_resized), vmax=np.max(image_resized),
    interpolation = 'nearest',aspect = 'auto',origin='lower')
  #plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
  ax[1].set_ylabel('Frequency (muHz)')
  ax[1].set_xlabel('Frequency [dnu] (muHz)')
  ax[1].set_yticks(range(6))
  ax[1].set_yticklabels(np.arange(min_freq,max_freq,delta_step))
  ax[1].set_xticks(range(0,number_of_bin_for_dnu, np.int(number_of_bin_for_dnu/deltanu)))
  ax[1].set_xticklabels(range(np.int(deltanu)+1))
  #plt.savefig(PATH_SAVE+'echelle_diag_'+str(KIC)+'_'+str(bg)+'bg_Yu18.png')


  power6nu = power[np.where((freq>=(numax-6.0*deltanu)) & (freq <= (numax+6.0*deltanu)))]
  freq6nu = freq[np.where((freq>=(numax-6.0*deltanu)) & (freq <= (numax+6.0*deltanu)))]

  aa = construct_bg(np.transpose(freq6nu), fit_obj)


  ax[1].plot(freq6nu, power6nu/aa)
  ax[1].axvline(numax,color='red')
  ax[1].set_ylabel(r'PSD / background')
  ax[1].set_xlabel(r'Frequency ($\mu Hz$)')
  plt.savefig(PATH_SAVE2+'echelle_spectre_'+str(KIC)+'_'+str(bg)+'bg_fixl0_Yu18.png')
  #plt.show()


  #print('Do you want to continue ?')
  #rep = input('Y / N ')
  #if rep == 'N' :
  # break


# Open file containing numax, delta nu (Yu 2018)
with open('/Users/students/Arthur/depressed_RG/RG_Kepler_Yu2018.csv','r') as txtfile :
  extra_param = csv.reader(txtfile, dialect='excel',delimiter=',')

  next(extra_param)

  KIC_param, numaxYu, deltanuYu = [], [], []

  for row in extra_param:
    while '' in row:
      row.remove('')
    KIC_param += [[np.float(i) for i in row][0]]
    deltanuYu += [[np.float(i) for i in row][5]]
    numaxYu += [[np.float(i) for i in row][4]]


# Function to look for matching KIC ID between two files
match = lambda a, b:[[a.index(x),b.index(x)] if (x in b and x in a) else [None,None] for x in a]

KIC_index = match(KICtab,KIC_param)
#print(KIC_index)

numaxYu    =  [numaxYu[x] for pos, x in enumerate([row[1] for row in KIC_index]) if x is not None]
deltanuYu  =  [deltanuYu[x] for pos, x in enumerate([row[1] for row in KIC_index]) if x is not None]
numaxtab   =  [numaxtab[x] for pos, x in enumerate([row[0] for row in KIC_index]) if x is not None]
deltanutab =  [deltanutab[x] for pos, x in enumerate([row[0] for row in KIC_index]) if x is not None]
freqtab    =  [freqtab[x] for pos, x in enumerate([row[0] for row in KIC_index]) if x is not None]
powertab   =  [powertab[x] for pos, x in enumerate([row[0] for row in KIC_index]) if x is not None]
KICtab     =  [KICtab[x] for pos, x in enumerate([row[0] for row in KIC_index]) if x is not None]


np.savetxt(PATH_SAVE+'NuMax_comparison.csv', np.c_[KICtab,numaxtab,numaxYu,deltanutab,deltanuYu],
  delimiter=',',fmt=['%-s','%-s','%-s','%-s','%-s'], header="KICtab,numaxtab,numaxYu,deltanutab,deltanuYu", comments='')


for ii in range(len(KICtab)):
  numax = numaxYu[ii]
  deltanu = deltanuYu[ii]
  power3nu = powertab[ii][np.where((freqtab[ii]>=(numax-3.0*deltanu)) & (freqtab[ii] <= (numax+3.0*deltanu)))]
  freq3nu = freqtab[ii][np.where((freqtab[ii]>=(numax-3.0*deltanu)) & (freqtab[ii] <= (numax+3.0*deltanu)))]

  aa = construct_bg(np.transpose(freq3nu), fit_obj)



fig, ax = plt.subplots()
#plt.axis('on')
p = ax.pcolor(image_resized, cmap='binary', vmin=np.min(image_resized), vmax=np.max(image_resized))
plt.subplots_adjust(left=0, right=1, top=1, bottom=0)
plt.ylabel('Frequency (muHz)')
plt.xlabel('Frequency [dnu] (muHz)')
plt.savefig(PATH_SAVE+'echelle_diag_'+str(KIC)+'_'+str(bg)+'bg.png')
plt.show()
'''
